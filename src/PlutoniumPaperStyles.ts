/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */

import { css } from 'lit-element';

export const initialeStyles = css`
  --plutonium-initiale-default-icon-size: 32px;
  --plutonium-initiale-secondary-color: magenta;
  --plutonium-initiale-background-color: white;

  .plutonium-initiale {
    border-radius: calc(var(--plutonium-initiale-default-icon-size) * (3 / 4));
    width: var(--plutonium-initiale-default-icon-size);
    height: var(--plutonium-initiale-default-icon-size);
    line-height: var(--plutonium-initiale-default-icon-size);
    background-color: var(--plutonium-initiale-background-color);
    color: var(--plutonium-initiale-secondary-color);
    display: inline-block;
    text-align: center;
    margin-right: calc(var(--plutonium-initiale-default-icon-size) / 8);
  }
`

export const floatingActionStyles = css`
  .plutonium.palette {
    position: fixed;
    bottom: 32px;
    right: 32px;
  }

  .plutonium.palette .plutonium.action-container {
    position: relative;
  }

  .plutonium.palette .plutonium.action-container .plutonium.action-control {
    position: relative;
    margin-right: 8px;
  }
`
