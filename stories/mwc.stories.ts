/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */
import '../plutonium-paper-styles.js'
import '@material/mwc-button'
import '@material/mwc-checkbox'
import '@material/mwc-circular-progress'
import '@material/mwc-dialog'
import '@material/mwc-drawer'
import '@material/mwc-fab'
import '@material/mwc-formfield'
import '@material/mwc-icon-button-toggle'
import '@material/mwc-icon-button'
import '@material/mwc-icon'
import '@material/mwc-linear-progress'
import '@material/mwc-list'
import '@material/mwc-menu'
import '@material/mwc-radio'
import '@material/mwc-select'
import '@material/mwc-slider'
import '@material/mwc-snackbar'
import '@material/mwc-switch'
import '@material/mwc-tab-bar'
import '@material/mwc-tab'
import '@material/mwc-textfield'
import '@material/mwc-textarea'
import '@material/mwc-top-app-bar'
import '@material/mwc-top-app-bar-fixed'

/*
 *   Copyright (c) 2021 László Hegedűs
 *   All rights reserved.
 */
import { TemplateResult, html } from 'lit-html'

export default {
  title: 'Material / Components',
  component: 'plutonium-styles',
  argTypes: {
    title: { control: 'text' },
    counter: { control: 'number' },
    textColor: { control: 'color' }
  }
}

interface Story<T> {
  (args: T): TemplateResult
  args?: Partial<T>
  argTypes?: Record<string, unknown>
}

interface ArgTypes {
  title?: string
  counter?: number
  textColor?: string
  slot?: TemplateResult
}

const MwcButtonTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <h2>Standard</h2>
  <div class="demo-group wrap">
    <mwc-button label="standard"></mwc-button>
    <mwc-button label="standard" icon="code"></mwc-button>
  </div>

  <h2>Outlined</h2>
  <div class="demo-group wrap">
    <mwc-button outlined label="outlined"></mwc-button>
    <mwc-button outlined label="outlined" icon="code"></mwc-button>
  </div>

  <h2>Raised</h2>
  <div class="demo-group wrap">
    <mwc-button raised label="raised"></mwc-button>
    <mwc-button raised label="raised" icon="code"></mwc-button>
  </div>

  <h2>Unelevated</h2>
  <div class="demo-group wrap">
    <mwc-button unelevated label="unelevated"></mwc-button>
    <mwc-button unelevated label="unelevated" icon="code"></mwc-button>
  </div>

  <h2>Dense</h2>
  <div class="demo-group wrap">
    <mwc-button dense unelevated label="dense"></mwc-button>
    <mwc-button dense unelevated label="dense" icon="code"></mwc-button>
  </div>

  <h2>Trailing icon</h2>
  <div class="demo-group wrap">
    <mwc-button label="trailing icon" icon="code" trailingIcon></mwc-button>
    <mwc-button
      outlined
      label="trailing icon"
      icon="code"
      trailingIcon
    ></mwc-button>
    <mwc-button
      raised
      label="trailing icon"
      icon="code"
      trailingIcon
    ></mwc-button>
    <mwc-button
      unelevated
      label="trailing icon"
      icon="code"
      trailingIcon
    ></mwc-button>
  </div>

  <h2>Disabled</h2>
  <div class="demo-group wrap">
    <mwc-button disabled label="disabled" icon="code"></mwc-button>
    <mwc-button disabled outlined label="disabled" icon="code"></mwc-button>
    <mwc-button disabled raised label="disabled" icon="code"></mwc-button>
    <mwc-button disabled unelevated label="disabled" icon="code"></mwc-button>
  </div>

  <h2>Full Width</h2>
  <div class="demo-group wrap">
    <mwc-button fullwidth outlined label="full-width"></mwc-button>
  </div>

  <h2>Styling</h2>
  <style data-pre="main-colors">
    .main-colors {
      --mdc-theme-primary: darkgreen;
      --mdc-theme-on-primary: aqua;
      --mdc-button-disabled-fill-color: darkseagreen;
      --mdc-button-disabled-ink-color: aliceblue;
    }
  </style>
  <pre id="main-colors"></pre>
  <div class="demo-group wrap main-colors">
    <mwc-button icon="code" label="standard"></mwc-button>
    <mwc-button outlined icon="code" label="outlined"></mwc-button>
    <mwc-button raised icon="code" label="raised"></mwc-button>
    <mwc-button
      raised
      icon="code"
      label="raised-disabled"
      disabled
    ></mwc-button>
    <mwc-button unelevated icon="code" label="unelevated"></mwc-button>
    <mwc-button
      unelevated
      icon="code"
      label="unelevated-disabled"
      disabled
    ></mwc-button>
  </div>

  <style data-pre="text">
    .text {
      --mdc-typography-button-text-transform: lowercase;
      --mdc-typography-button-letter-spacing: 4px;
    }
  </style>
  <pre id="text"></pre>
  <div class="demo-group wrap text">
    <mwc-button icon="code" label="lowerspace"></mwc-button>
    <mwc-button outlined icon="code" label="lowerspace"></mwc-button>
    <mwc-button raised icon="code" label="lowerspace"></mwc-button>
    <mwc-button unelevated icon="code" label="lowerspace"></mwc-button>
  </div>

  <style data-pre="horizontal-padding">
    .horizontal-padding {
      --mdc-button-horizontal-padding: 50px;
    }
  </style>
  <pre id="horizontal-padding"></pre>
  <div class="demo-group wrap horizontal-padding">
    <mwc-button icon="code" label="padding"></mwc-button>
    <mwc-button outlined icon="code" label="padding"></mwc-button>
    <mwc-button raised icon="code" label="padding"></mwc-button>
    <mwc-button unelevated icon="code" label="padding"></mwc-button>
  </div>

  <style data-pre="outline">
    .outline .themed {
      --mdc-theme-primary: royalblue;
      --mdc-button-disabled-ink-color: powderblue;
    }

    .outline .outline-different {
      --mdc-button-outline-color: darkgreen;
      --mdc-button-disabled-outline-color: palegreen;
      --mdc-button-disabled-ink-color: plum;
    }

    .outline .width {
      --mdc-button-outline-width: 5px;
    }
  </style>
  <pre id="outline"></pre>
  <div class="demo-group wrap outline">
    <mwc-button outlined class="themed" icon="code" label="themed"></mwc-button>
    <mwc-button
      outlined
      class="themed"
      disabled
      icon="code"
      label="themed"
    ></mwc-button>
    <mwc-button
      outlined
      class="outline-different"
      icon="code"
      label="outline-different"
    ></mwc-button>
    <mwc-button
      outlined
      class="outline-different"
      disabled
      icon="code"
      label="outline-different"
    ></mwc-button>
    <mwc-button outlined class="width" icon="code" label="width"></mwc-button>
    <mwc-button
      outlined
      class="width"
      disabled
      icon="code"
      label="width"
    ></mwc-button>
  </div>
`

export const Buttons = MwcButtonTemplate.bind({})

const CheckboxesTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
<mwc-checkbox></mwc-checkbox>
<mwc-checkbox checked></mwc-checkbox>
<mwc-checkbox indeterminate></mwc-checkbox>
<mwc-checkbox disabled></mwc-checkbox>
</div>
<div>
<span class="styled demo-group">
  <mwc-checkbox></mwc-checkbox>
  <mwc-checkbox checked></mwc-checkbox>
  <mwc-checkbox indeterminate></mwc-checkbox>
  <mwc-checkbox disabled></mwc-checkbox>
</span>
`

export const Checkboxes = CheckboxesTemplate.bind({})

const CircularProgressTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
<mwc-button id="toggle" raised ripple label="Toggle"></mwc-button>
<mwc-circular-progress progress=0.4 id="progress1"></mwc-circular-progress>
<h3>circular-progress: determinate</h3>
<mwc-circular-progress progress="0.7"></mwc-circular-progress>
<h3>circular-progress: indeterminate</h3>
<mwc-circular-progress indeterminate></mwc-circular-progress>
<h3>circular-progress: custom color</h3>
<mwc-circular-progress indeterminate class="demo-progress-bar"></mwc-circular-progress>
<h3>circular-progress: custom four color</h3>
<mwc-circular-progress-four-color indeterminate class="demo-progress-bar-four-color"></mwc-circular-progress>
`

export const CircularProgress = CircularProgressTemplate.bind({})

const MwcDialogTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-button data-num="1" raised>Basic</mwc-button>
  <mwc-dialog id="dialog1" heading="Dialog header">
    Dialog body text
    <mwc-button slot="primaryAction" dialogAction="ok">Action 2</mwc-button>
    <mwc-button slot="secondaryAction" dialogAction="cancel"
      >Action 1</mwc-button
    >
  </mwc-dialog>

  <mwc-button data-num="2" raised>Actions</mwc-button>
  <mwc-dialog id="dialog2" heading="Actions">
    <p>
      By setting the dialogAction="my-action" attribute on any element projected
      into mwc-dialog, you can close the dialog by clicking on that element. The
      dialog will then fire a non-bubbling "closing" event and a non-bubbling
      "closed" event with an event detail of {action: "my-action"}
    </p>
    <mwc-button slot="primaryAction" dialogAction="customAction"
      >This has action</mwc-button
    >
    <mwc-button slot="secondaryAction">this does not</mwc-button>
  </mwc-dialog>

  <mwc-button data-num="3" raised>Scrollable</mwc-button>
  <mwc-dialog id="dialog3" heading="My Title">
    <p>
      Really long text will scroll. Really long text will scroll. Really long
      text will scroll. Really long text will scroll. Really long text will
      scroll. Really long text will scroll. Really long text will scroll. Really
      long text will scroll. Really long text will scroll. Really long text will
      scroll. Really long text will scroll. Really long text will scroll. Really
      long text will scroll. Really long text will scroll. Really long text will
      scroll. Really long text will scroll. Really long text will scroll. Really
      long text will scroll. Really long text will scroll. Really long text will
      scroll. Really long text will scroll. Really long text will scroll. Really
      long text will scroll. Really long text will scroll. Really long text will
      scroll. Really long text will scroll. Really long text will scroll. Really
      long text will scroll. Really long text will scroll. Really long text will
      scroll. Really long text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll. Really log text will
      scroll. Really log text will scroll. Really log text will scroll. Really
      log text will scroll. Really log text will scroll.
    </p>
    <mwc-button slot="primaryAction" dialogAction="close"
      >Close this!</mwc-button
    >
  </mwc-dialog>

  <mwc-button data-num="4" raised>Hide Actions</mwc-button>
  <mwc-dialog id="dialog4" heading="Hide Actions">
    <p>
      If you don't have actions, you may want to set the "hideActions" property.
      This property will remove extra whitespace at the bottom of this dialog.
      This button will toggle that whitespace:
    </p>
    <mwc-button raised id="toggleActions">Toggle hideActions</mwc-button>
    <div>
      mwc-dialog.hideActions is currently:
      <span id="hideActionVal">
        false
      </span>
    </div>
  </mwc-dialog>

  <style>
    .styled {
      --mdc-theme-surface: #fff;
      --mdc-dialog-scrim-color: rgba(35, 47, 52, 0.32);
      --mdc-dialog-heading-ink-color: #232f34;
      --mdc-dialog-content-ink-color: #232f34;
      --mdc-dialog-scroll-divider-color: transparent;
      --mdc-dialog-min-width: 500px;
      --mdc-dialog-max-width: 700px;
      --mdc-dialog-max-height: 350px;
      --mdc-dialog-shape-radius: 0px;

      /* color buttons */
      --mdc-theme-primary: #344955;
    }
  </style>

  <mwc-button data-num="5" raised>Styled</mwc-button>
  <mwc-dialog id="dialog5" heading="Styled" class="styled">
    <div>These are the current styles applied to this dialog</div>
    <pre>
--mdc-theme-surface: #FFF;
--mdc-dialog-scrim-color: rgba(35, 47, 52, .32);
--mdc-dialog-heading-ink-color: #232F34;
--mdc-dialog-content-ink-color: #232F34;
--mdc-dialog-scroll-divider-color: transparent;
--mdc-dialog-min-width: 500px;
--mdc-dialog-max-width: 700px;
--mdc-dialog-max-height: 350px;
--mdc-dialog-shape-radius: 0px;

/* color buttons */
--mdc-theme-primary: #344955;
    </pre
    >
    <mwc-button slot="primaryAction" dialogAction="close">
      Too stylish for me!
    </mwc-button>
  </mwc-dialog>

  <mwc-button data-num="6" raised>Stacked</mwc-button>
  <mwc-dialog id="dialog6" heading="Stacked" stacked>
    <div>
      This is what happens when you set the stacked property on mwc-dialog.
      Notice that the primary action is now on top.
    </div>
    <mwc-button slot="primaryAction" dialogAction="close">
      Primary
    </mwc-button>
    <mwc-button slot="secondaryAction" dialogAction="close">
      Secondary
    </mwc-button>
  </mwc-dialog>

  <mwc-button data-num="7" raised>Initial Focus</mwc-button>
  <mwc-dialog id="dialog7" heading="Initial Focus">
    <div>
      In this example we set "dialogInitialFocus" on the mwc-textfield. When
      this dialog opens, it is auto-focused.
    </div>
    <mwc-textfield label="i am auto-focused" dialogInitialFocus>
    </mwc-textfield>
    <mwc-button slot="primaryAction" dialogAction="close">
      Primary
    </mwc-button>
    <mwc-button slot="secondaryAction" dialogAction="close">
      Secondary
    </mwc-button>
  </mwc-dialog>

  <mwc-button data-num="8" raised>Form Validation</mwc-button>
  <mwc-dialog id="dialog8" heading="Form Validation">
    <p>This dialog can validate user input before closing.</p>
    <mwc-textfield
      id="dialog8-text-field"
      minlength="3"
      maxlength="64"
      placeholder="First name"
      required
    >
    </mwc-textfield>
    <mwc-button id="dialog8-primary-action-button" slot="primaryAction">
      Confirm
    </mwc-button>
    <mwc-button slot="secondaryAction" dialogAction="close">
      Cancel
    </mwc-button>
  </mwc-dialog>
`

export const Dialogs = MwcDialogTemplate.bind({})

const MwcDrawerTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-drawer hasHeader type="dismissible">
    <span slot="title">Drawer Title</span>
    <span slot="subtitle">subtitle</span>
    <div class="drawer-content">
      <p>Drawer content</p>
      <mwc-icon-button icon="gesture"></mwc-icon-button>
      <mwc-icon-button icon="gavel"></mwc-icon-button>
    </div>
    <div slot="appContent">
      <mwc-top-app-bar>
        <mwc-icon-button slot="navigationIcon" icon="menu"></mwc-icon-button>
        <div slot="title">Title</div>
      </mwc-top-app-bar>
      <div class="main-content">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
    </div>
  </mwc-drawer>
`

export const Drawers = MwcDrawerTemplate.bind({})

const MwcFabTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-fab icon="edit"></mwc-fab>
  <mwc-fab icon="add" mini></mwc-fab>
  <mwc-fab icon="shopping_cart" extended label="Add To Cart"></mwc-fab>
  <mwc-fab icon="share" class="blackAndWhite"></mwc-fab>
`

export const FloatingActionButtons = MwcFabTemplate.bind({})

const MwcFormFieldTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-formfield label="This is a checkbox.">
    <mwc-checkbox></mwc-checkbox>
  </mwc-formfield>

  <h4>Align End</h4>
  <mwc-formfield alignEnd label="This is a checkbox.">
    <mwc-checkbox></mwc-checkbox>
  </mwc-formfield>

  <h4>Native Element</h4>
  <mwc-formfield alignEnd label="Enter a date.">
    <input type="date" />
  </mwc-formfield>

  <h4>Switch</h4>
  <mwc-formfield label="This is a switch.">
    <mwc-switch></mwc-switch>
  </mwc-formfield>

  <h4>Radio Button</h4>
  <mwc-formfield label="This is a radio.">
    <mwc-radio id="one" name="a"></mwc-radio>
  </mwc-formfield>
  <mwc-formfield label="This is a radio.">
    <mwc-radio id="two" name="a" checked></mwc-radio>
  </mwc-formfield>
  <mwc-formfield label="This is a radio.">
    <mwc-radio id="three" name="a"></mwc-radio>
  </mwc-formfield>
`

export const FormFields = MwcFormFieldTemplate.bind({})

const MwcIconButtonToggleTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-icon-button-toggle
    onIcon="sentiment_very_satisfied"
    offIcon="sentiment_very_dissatisfied"
  ></mwc-icon-button-toggle>

  <mwc-icon-button-toggle>
    <svg
      slot="onIcon"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <path d="M0 0h24v24H0z" fill="none" />
      <path
        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
      />
    </svg>
    <svg
      slot="offIcon"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <path fill="none" d="M0 0h24v24H0V0zm0 0h24v24H0V0z" />
      <path
        d="M16.59 7.58L10 14.17l-3.59-3.58L5 12l5 5 8-8zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"
      />
    </svg>
  </mwc-icon-button-toggle>

  <mwc-icon-button-toggle>
    <img slot="onIcon" src="https://picsum.photos/id/28/24/24" />
    <img slot="offIcon" src="https://picsum.photos/id/141/24/24?grayscale" />
  </mwc-icon-button-toggle>

  <mwc-icon-button-toggle
    disabled
    onIcon="sentiment_very_satisfied"
    offIcon="sentiment_very_dissatisfied"
  ></mwc-icon-button-toggle>

  <mwc-icon-button-toggle
    class="color"
    onIcon="sentiment_very_satisfied"
    offIcon="sentiment_very_dissatisfied"
  ></mwc-icon-button-toggle>
`

export const IconButtonToggles = MwcIconButtonToggleTemplate.bind({})

const MwcIconButtonTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-icon-button icon="code"></mwc-icon-button>
  <mwc-icon-button>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <path d="M0 0h24v24H0z" fill="none" />
      <path
        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
      />
    </svg>
  </mwc-icon-button>
  <mwc-icon-button icon="code" disabled></mwc-icon-button>
  <mwc-icon-button icon="code" class="color"></mwc-icon-button>
`

export const IconButtons = MwcIconButtonTemplate.bind({})

const MwcIconTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <div class="demo-group">
    <mwc-icon>sentiment_very_dissatisfied</mwc-icon>
    <mwc-icon>sentiment_dissatisfied</mwc-icon>
    <mwc-icon>sentiment_neutral</mwc-icon>
    <mwc-icon>sentiment_satisfied</mwc-icon>
    <mwc-icon>sentiment_very_satisfied</mwc-icon>
  </div>

  <h4>color and size</h4>
  <div class="demo-group color-size">
    <mwc-icon>all_out</mwc-icon>
    <mwc-icon>accessibility</mwc-icon>
    <mwc-icon>exit_to_app</mwc-icon>
    <mwc-icon>camera</mwc-icon>
  </div>
`

export const Icons = MwcIconTemplate.bind({})

const MwcLinearProgressTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-button id="toggle" raised ripple label="Toggle"></mwc-button>
  <mwc-linear-progress id="progress1"></mwc-linear-progress>
  <h3>linear-progress: reverse</h3>
  <mwc-linear-progress
    reverse
    progress="0.4"
    class="demo-progress-bar"
  ></mwc-linear-progress>
  <h3>linear-progress: determinate</h3>
  <mwc-linear-progress
    determinate
    progress="0.5"
    buffer="1"
  ></mwc-linear-progress>
  <h3>linear-progress: determinate buffer</h3>
  <mwc-linear-progress
    determinate
    progress="0.3"
    buffer="0.7"
    class="demo-progress-bar"
  ></mwc-linear-progress>
  <h3>linear-progress: determinate buffer reverse</h3>
  <mwc-linear-progress
    determinate
    reverse
    progress="0.2"
    buffer="0.6"
  ></mwc-linear-progress>
`

export const LinearProgresses = MwcLinearProgressTemplate.bind({})

const MwcListTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <h1>Basic</h1>

  <mwc-list id="basic">
    <mwc-list-item selected>Item 0</mwc-list-item>
    <mwc-list-item>Item 1</mwc-list-item>
    <mwc-list-item>Item 2</mwc-list-item>
    <mwc-list-item>Item 3</mwc-list-item>
  </mwc-list>

  <div>Selected index: <span id="basicIndex"></span></div>

  <h1>Multi</h1>

  <mwc-list multi id="multi">
    <mwc-list-item selected>Item 0</mwc-list-item>
    <mwc-list-item>Item 1</mwc-list-item>
    <mwc-list-item selected>Item 2</mwc-list-item>
    <mwc-list-item>Item 3</mwc-list-item>
  </mwc-list>

  <div>Selected index (Set&lt;number&gt;): <span id="multiIndex"></span></div>

  <h1>Activatable</h1>

  <mwc-list activatable id="activatable">
    <mwc-list-item>Item 0</mwc-list-item>
    <mwc-list-item selected activated>Item 1</mwc-list-item>
    <mwc-list-item>Item 2</mwc-list-item>
    <mwc-list-item>Item 3</mwc-list-item>
  </mwc-list>

  <div>Selected index: <span id="activatableIndex"></span></div>

  <h1>Non-interactive</h1>

  <mwc-list noninteractive>
    <mwc-list-item>Item 0</mwc-list-item>
    <mwc-list-item>Item 1</mwc-list-item>
    <mwc-list-item>Item 2</mwc-list-item>
    <mwc-list-item>Item 3</mwc-list-item>
  </mwc-list>

  <h1>Checklist</h1>

  <mwc-list multi id="checklist">
    <mwc-check-list-item selected>Item 0</mwc-check-list-item>
    <mwc-check-list-item>Item 1</mwc-check-list-item>
    <mwc-check-list-item left selected>Item 2 (left)</mwc-check-list-item>
    <mwc-check-list-item left>Item 3 (left)</mwc-check-list-item>
    <mwc-check-list-item disabled><span>disabled</span></mwc-check-list-item>
  </mwc-list>

  <div>
    Selected index (Set&lt;number&gt;): <span id="checklistIndex"></span>
  </div>

  <h1>Radio list</h1>

  <mwc-list id="radio">
    <mwc-radio-list-item group="a" selected>Item 0</mwc-radio-list-item>
    <mwc-radio-list-item group="a">Item 1</mwc-radio-list-item>
    <mwc-radio-list-item left group="a">Item 2 (left)</mwc-radio-list-item>
    <mwc-radio-list-item left group="a">Item 3 (left)</mwc-radio-list-item>
  </mwc-list>

  <div>Selected index: <span id="radioIndex"></span></div>

  <h1>Multi Radio list (with divider)</h1>

  <mwc-list multi id="multiRadio">
    <mwc-radio-list-item group="b">Item 0</mwc-radio-list-item>
    <mwc-radio-list-item group="b" selected>Item 1</mwc-radio-list-item>
    <li divider role="separator"></li>
    <mwc-radio-list-item group="c" selected>Item 2</mwc-radio-list-item>
    <mwc-radio-list-item group="c">Item 3</mwc-radio-list-item>
  </mwc-list>

  <div>
    Selected index (Set&lt;number&gt;): <span id="multiRadioIndex"></span>
  </div>

  <h1>Two-line (padded dividers)</h1>

  <mwc-list multi>
    <mwc-list-item twoline>
      <span>Item 0</span>
      <span slot="secondary">Secondary line</span>
    </mwc-list-item>
    <li divider padded role="separator"></li>
    <mwc-list-item twoline>
      <span>Item 1</span>
      <span slot="secondary">Secondary line</span>
    </mwc-list-item>
    <li divider padded role="separator"></li>
    <mwc-list-item twoline>
      <span>Item 2</span>
      <span slot="secondary">Secondary line</span>
    </mwc-list-item>
    <li divider padded role="separator"></li>
    <mwc-list-item twoline>
      <span>Item 3</span>
      <span slot="secondary">Secondary line</span>
    </mwc-list-item>
  </mwc-list>

  <h1>Graphics (inset dividers)</h1>

  <mwc-list id="graphics" multi>
    <mwc-list-item twoline graphic="avatar" noninteractive hasMeta>
      <span>Non-interactive two line avatar graphic</span>
      <span slot="secondary">email@domain.tld</span>
      <span slot="graphic" class="material-icons inverted">folder</span>
      <span slot="meta" class="material-icons">info</span>
    </mwc-list-item>
    <li divider role="separator"></li>
    <mwc-list-item graphic="icon" hasMeta>
      <span>Icon graphic</span>
      <span slot="graphic" class="material-icons inverted">folder</span>
      <span slot="meta" class="material-icons">info</span>
    </mwc-list-item>
    <li divider inset role="separator"></li>
    <mwc-list-item graphic="medium" hasMeta>
      <span>medium graphic</span>
      <span slot="graphic" class="material-icons inverted">folder</span>
      <span slot="meta" class="material-icons">info</span>
    </mwc-list-item>
    <li divider inset role="separator"></li>
    <mwc-list-item graphic="large" hasMeta>
      <span>large graphic</span>
      <span slot="graphic" class="material-icons inverted">folder</span>
      <span slot="meta" class="material-icons">info</span>
    </mwc-list-item>
  </mwc-list>

  <div>
    Selected index (Set&lt;number&gt;): <span id="graphicsIndex"></span>
  </div>

  <h1>Styled</h1>

  <style>
    #styled {
      --mdc-theme-primary: red;
      --mdc-list-vertical-padding: 0px;
      --mdc-list-side-padding: 0px;
    }
  </style>

  <mwc-list activatable id="styled">
    <mwc-list-item selected>Item 0</mwc-list-item>
    <mwc-list-item>Item 1</mwc-list-item>
    <mwc-list-item>Item 2</mwc-list-item>
    <mwc-list-item>Item 3</mwc-list-item>
  </mwc-list>

  <h1>Styled (Ripple Disabled)</h1>

  <style>
    /* disable ripple */
    #styledr {
      --mdc-ripple-color: transparent;
    }

    #styledr > * {
      transition: background-color 0.2s, color 0.2s;
    }

    #styledr [selected] {
      background-color: rgb(33, 33, 33);
      color: white;
    }

    #styledr [mwc-list-item]:not([selected]):hover,
    #styledr [mwc-list-item]:not([selected]):focus {
      background-color: rgba(33, 33, 33, 0.3);
    }

    #styledr [mwc-list-item]:not([selected]):active {
      background-color: rgba(33, 33, 33, 0.4);
    }

    #styledr [mwc-list-item][selected]:hover,
    #styledr [mwc-list-item][selected]:focus {
      background-color: rgba(33, 33, 33, 0.9);
    }

    #styledr [mwc-list-item][selected]:active {
      background-color: rgba(33, 33, 33, 0.8);
    }
  </style>

  <mwc-list id="styledr">
    <mwc-list-item selected>Item 0</mwc-list-item>
    <mwc-list-item>Item 1</mwc-list-item>
    <mwc-list-item>Item 2</mwc-list-item>
    <mwc-list-item>Item 3</mwc-list-item>
  </mwc-list>
`

export const Lists = MwcListTemplate.bind({})

const MwcMenuTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <div style="position:relative;">
    <mwc-button id="basicButton" raised label="Open Basic Menu"></mwc-button>
    <mwc-menu id="basicMenu">
      <mwc-list-item>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>
  </div>

  <div style="position:relative;">
    <mwc-button id="cornerButton" raised label="Open Menu With Corner">
    </mwc-button>
    <mwc-menu id="cornerMenu">
      <mwc-list-item>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>

    <mwc-select outlined id="cornerSelect" label="Menu Corner">
      <mwc-list-item selected></mwc-list-item>
      <mwc-list-item value="TOP_LEFT">TOP_LEFT</mwc-list-item>
      <mwc-list-item value="TOP_RIGHT">TOP_RIGHT</mwc-list-item>
      <mwc-list-item value="BOTTOM_LEFT">BOTTOM_LEFT</mwc-list-item>
      <mwc-list-item value="BOTTOM_RIGHT">BOTTOM_RIGHT</mwc-list-item>
      <mwc-list-item value="TOP_START">TOP_START</mwc-list-item>
      <mwc-list-item value="TOP_END">TOP_END</mwc-list-item>
      <mwc-list-item value="BOTTOM_START">BOTTOM_START</mwc-list-item>
      <mwc-list-item value="BOTTOM_END">BOTTOM_END</mwc-list-item>
    </mwc-select>
  </div>

  <div style="position:relative;">
    <mwc-button id="quickButton" raised label="Open Quick Menu"></mwc-button>
    <mwc-menu quick id="quickMenu">
      <mwc-list-item>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>
  </div>

  <div class="scrollable">
    <span>
      <mwc-button id="fixedButton" raised label="Open Fixed Menu"></mwc-button>
      <mwc-menu fixed id="fixedMenu">
        <mwc-list-item>one</mwc-list-item>
        <mwc-list-item>two</mwc-list-item>
        <mwc-list-item>three</mwc-list-item>
        <mwc-list-item disabled><div>four</div></mwc-list-item>
        <li divider></li>
        <mwc-list-item>aaa</mwc-list-item>
        <mwc-list-item>bbb</mwc-list-item>
      </mwc-menu>
    </span>

    <span style="position:relative;">
      <mwc-button
        id="nonfixedButton"
        raised
        label="Open Non-Fixed Menu"
      ></mwc-button>
      <mwc-menu id="nonfixedMenu">
        <mwc-list-item>one</mwc-list-item>
        <mwc-list-item>two</mwc-list-item>
        <mwc-list-item>three</mwc-list-item>
        <mwc-list-item disabled><div>four</div></mwc-list-item>
        <li divider></li>
        <mwc-list-item>aaa</mwc-list-item>
        <mwc-list-item>bbb</mwc-list-item>
      </mwc-menu>
    </span>
    <div>Open each menu and then scroll this div</div>
    <div class="spacer"></div>
  </div>

  <div>
    <mwc-button
      id="absoluteButton"
      raised
      label="Open Absolute Menu (no anchor)"
    ></mwc-button>
    <mwc-menu absolute x="0" y="0" id="absoluteMenu">
      <mwc-list-item>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>

    <mwc-textfield type="number" label="x" outlined id="absoluteX" value="0">
    </mwc-textfield>
    <mwc-textfield type="number" label="y" outlined id="absoluteY" value="0">
    </mwc-textfield>
  </div>

  <div style="position:relative;">
    <mwc-button
      id="activatableButton"
      raised
      label="Open Activatable Menu"
    ></mwc-button>
    <mwc-menu activatable id="activatableMenu">
      <mwc-list-item>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>
  </div>

  <div style="position:relative;">
    <mwc-button
      id="multiButton"
      raised
      label="Open Multi (activatable) Menu"
    ></mwc-button>
    <mwc-menu multi activatable id="multiMenu">
      <mwc-list-item selected activated>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item selected activated>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>
  </div>

  <div style="position:relative;">
    <style>
      #groupedMenu mwc-list-item:not([selected]) mwc-icon {
        display: none;
      }
    </style>

    <mwc-button
      id="groupedButton"
      raised
      label="Open Grouped Menu"
    ></mwc-button>
    <mwc-menu multi id="groupedMenu">
      <mwc-list-item selected group="a" graphic="icon">
        <mwc-icon slot="graphic">check</mwc-icon>
        <span>a-one</span>
      </mwc-list-item>
      <mwc-list-item group="a" graphic="icon">
        <mwc-icon slot="graphic">check</mwc-icon>
        <span>a-two</span>
      </mwc-list-item>
      <mwc-list-item group="a" graphic="icon">
        <mwc-icon slot="graphic">check</mwc-icon>
        <span>a-three</span>
      </mwc-list-item>
      <li divider></li>
      <mwc-list-item selected group="b" graphic="icon">
        <mwc-icon slot="graphic">check</mwc-icon>
        <span>b-one</span>
      </mwc-list-item>
      <mwc-list-item group="b" graphic="icon">
        <mwc-icon slot="graphic">check</mwc-icon>
        <span>b-two</span>
      </mwc-list-item>
    </mwc-menu>
  </div>

  <div style="position:relative;">
    <mwc-button id="dfsButton" raised label="Open Menu With Default Focus">
    </mwc-button>
    <mwc-menu id="dfsMenu">
      <mwc-list-item>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>

    <mwc-select outlined id="dfsSelect" label="Menu Default Focus">
      <mwc-list-item value="NONE">NONE</mwc-list-item>
      <mwc-list-item value="LIST_ROOT" selected>LIST_ROOT</mwc-list-item>
      <mwc-list-item value="FIRST_ITEM">FIRST_ITEM</mwc-list-item>
      <mwc-list-item value="LAST_ITEM">LAST_ITEM</mwc-list-item>
    </mwc-select>
  </div>

  <div>
    <h3>
      Absolute w/ anchor
    </h3>
    <div>This is reusing the same menu and changing the anchor</div>
    <mwc-button id="absoluteLeftButton" raised label="anchor = this">
    </mwc-button>
    <mwc-button id="absoluteRightButton" raised label="anchor = this">
    </mwc-button>
    <mwc-menu absolute id="absoluteAnchorMenu">
      <mwc-list-item>one</mwc-list-item>
      <mwc-list-item>two</mwc-list-item>
      <mwc-list-item>three</mwc-list-item>
      <mwc-list-item disabled><div>four</div></mwc-list-item>
      <li divider></li>
      <mwc-list-item>aaa</mwc-list-item>
      <mwc-list-item>bbb</mwc-list-item>
    </mwc-menu>
  </div>
`

export const Menus = MwcMenuTemplate.bind({})

const MwcRadioTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <mwc-radio id="a1" name="a" checked></mwc-radio>
  <mwc-radio id="a2" name="a"></mwc-radio>
  <mwc-radio id="a3" name="a" disabled></mwc-radio>
  <span class="styled">
    <mwc-radio id="b1" name="b"></mwc-radio>
    <mwc-radio id="b2" name="b" checked></mwc-radio>
    <mwc-radio id="b3" name="b" disabled></mwc-radio>
  </span>
`

export const Radios = MwcRadioTemplate.bind({})

const MwcSelectTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <h2>Filled</h2>
  <mwc-select label="filled" id="filled">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Option 1</mwc-list-item>
    <mwc-list-item value="2">Option 2</mwc-list-item>
    <mwc-list-item value="3">Option 3</mwc-list-item>
  </mwc-select>
  <div>Value: <span id="filledValue"></span></div>

  <h2>Outlined</h2>
  <mwc-select outlined label="outlined" id="outlined">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Option 1</mwc-list-item>
    <mwc-list-item value="2">Option 2</mwc-list-item>
    <mwc-list-item value="3">Option 3</mwc-list-item>
  </mwc-select>
  <div>Value: <span id="outlinedValue"></span></div>

  <h2>Preselected</h2>
  <mwc-select label="preselected" id="preselected">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Option 1</mwc-list-item>
    <mwc-list-item value="2" selected>Option 2</mwc-list-item>
    <mwc-list-item value="3">Option 3</mwc-list-item>
  </mwc-select>
  <div>Value: <span id="preselectedValue"></span></div>

  <h2>Icon</h2>
  <mwc-select label="has icon" icon="event">
    <mwc-list-item graphic="icon" value="1">Option 1</mwc-list-item>
    <mwc-list-item graphic="icon" value="2">Option 2</mwc-list-item>
    <mwc-list-item graphic="icon" value="3">Option 3</mwc-list-item>
  </mwc-select>

  <h2>Required</h2>
  <mwc-select required label="required filled" id="reqFilled">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Option 1</mwc-list-item>
    <mwc-list-item value="2">Option 2</mwc-list-item>
    <mwc-list-item value="3">Option 3</mwc-list-item>
  </mwc-select>
  <div>Value: <span id="reqFilledValue"></span></div>
  <div>Valid: <span id="reqFilledValid"></span></div>

  <mwc-select outlined required label="required outlined" id="reqOutlined">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Option 1</mwc-list-item>
    <mwc-list-item value="2">Option 2</mwc-list-item>
    <mwc-list-item value="3">Option 3</mwc-list-item>
  </mwc-select>
  <div>Value: <span id="reqOutlinedValue"></span></div>
  <div>Valid: <span id="reqOutlinedValid"></span></div>

  <h2>Disabled</h2>
  <mwc-select disabled label="disabled">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item disabled value="1">Option 1</mwc-list-item>
    <mwc-list-item value="2">Option 2</mwc-list-item>
    <mwc-list-item value="3">Option 3</mwc-list-item>
  </mwc-select>

  <h2>Disabled Options</h2>
  <mwc-select label="disabled" id="disabledOptions">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Option 1</mwc-list-item>
    <mwc-list-item disabled value="2"><div>Option 2</div></mwc-list-item>
    <mwc-list-item value="3">Option 3</mwc-list-item>
  </mwc-select>
  <div>Value: <span id="disabledOptionsValue"></span></div>

  <h2>Natural Menu Width</h2>

  <style>
    #naturalOptions {
      max-width: 200px;
    }
  </style>
  <mwc-select label="natural width (long)" id="naturalOptions">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Some Really Really Really long text</mwc-list-item>
    <mwc-list-item value="2">Some Really Really Really long text</mwc-list-item>
    <mwc-list-item value="3">Some Really Really Really long text</mwc-list-item>
  </mwc-select>
  <div>
    <mwc-button raised label="toggle naturalMenuWidth" id="naturalButton">
    </mwc-button>
  </div>
  <div>
    <pre>mwc-select.naturalMenuWidth</pre>
    value:
    <span id="naturalValue">false</span>
  </div>

  <mwc-select label="natural width (short)" id="shortNaturalOptions">
    <mwc-list-item></mwc-list-item>
    <mwc-list-item value="1">Short text</mwc-list-item>
    <mwc-list-item value="2">Short text</mwc-list-item>
    <mwc-list-item value="3">Short text</mwc-list-item>
  </mwc-select>
  <div>
    <mwc-button raised label="toggle naturalMenuWidth" id="shortNaturalButton">
    </mwc-button>
  </div>
  <div>
    <pre>mwc-select.naturalMenuWidth</pre>
    value:
    <span id="shortNaturalValue">false</span>
  </div>

  <div id="fullwidthParent">
    <mwc-select label="fullwidth" id="fullwidthOptions">
      <mwc-list-item></mwc-list-item>
      <mwc-list-item value="1">Short text</mwc-list-item>
      <mwc-list-item value="2">Short text</mwc-list-item>
      <mwc-list-item value="3">Short text</mwc-list-item>
    </mwc-select>
  </div>
  <div>
    <mwc-button raised label="toggle fullwidth" id="fullwidthButton">
    </mwc-button>
  </div>
  <div>
    <pre>mwc-select.fullwidth</pre>
    value:
    <span id="fullwidthValue">false</span>
  </div>
`

export const Selects = MwcSelectTemplate.bind({})

const MwcSliderTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <h3>Continuous</h3>
  <div class="demo-container">
    <mwc-slider></mwc-slider>
  </div>

  <h3>Discrete</h3>
  <div class="demo-container demo-colory">
    <mwc-slider pin markers max="50" value="10" step="5"></mwc-slider>
  </div>

  <h3>Disabled</h3>
  <div class="demo-container">
    <mwc-slider disabled></mwc-slider>
  </div>

  <div>Value from input: <span id="logInput"></span></div>
  <div>Value from change: <span id="logChange"></span></div>
`

export const Sliders = MwcSliderTemplate.bind({})

const MwcSnackbarTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <div class="demo-group">
    <mwc-button id="snack1" raised ripple label="DEFAULT"></mwc-button>
    <mwc-button id="snack2" raised ripple label="LEADING"></mwc-button>
    <mwc-button id="snack3" raised ripple label="STACKED"></mwc-button>
    <mwc-textfield
      id="textfield"
      label="snackbar timeout (ms)"
      helper="4000ms - 10000ms or -1"
      type="number"
      value="5000"
    >
    </mwc-textfield>
  </div>

  <mwc-snackbar id="snack" labelText="Can't send photo. Retry in 5 seconds.">
    <mwc-button id="actionButton" slot="action">RETRY</mwc-button>
    <mwc-icon-button
      id="iconButton"
      icon="close"
      slot="dismiss"
    ></mwc-icon-button>
  </mwc-snackbar>
`

export const Snackbars = MwcSnackbarTemplate.bind({})

const MwcSwitchTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
  <div class="demo-group">
    <mwc-switch></mwc-switch>
    <mwc-switch checked></mwc-switch>
    <mwc-switch disabled></mwc-switch>
    <mwc-switch disabled checked></mwc-switch>
  </div>
  <div class="demo-group">
    <span class="styled demo-group-spaced">
      <mwc-switch></mwc-switch>
      <mwc-switch checked></mwc-switch>
      <mwc-switch disabled></mwc-switch>
      <mwc-switch disabled checked></mwc-switch>
    </span>
  </div>
`

export const Switches = MwcSwitchTemplate.bind({})

const MwcTabsTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
<mwc-tab-bar>
<mwc-tab label="one"></mwc-tab>
<mwc-tab label="two"></mwc-tab>
<mwc-tab label="three"></mwc-tab>
</mwc-tab-bar>

<h3>Fading indicator</h3>
<mwc-tab-bar>
<mwc-tab label="one" isFadingIndicator></mwc-tab>
<mwc-tab label="two" isFadingIndicator></mwc-tab>
<mwc-tab label="three" isFadingIndicator></mwc-tab>
</mwc-tab-bar>

<h3>Min Width Tab</h3>
<mwc-tab-bar>
<mwc-tab label="one" minWidth></mwc-tab>
<mwc-tab label="two" minWidth></mwc-tab>
<mwc-tab label="three" minWidth></mwc-tab>
</mwc-tab-bar>

<h3>Min Width Indicator</h3>
<mwc-tab-bar>
<mwc-tab label="one" isMinWidthIndicator></mwc-tab>
<mwc-tab label="two" isMinWidthIndicator></mwc-tab>
<mwc-tab label="three" isMinWidthIndicator></mwc-tab>
</mwc-tab-bar>

<h3>Min Width Tab - Min Width Indicator</h3>
<mwc-tab-bar>
<mwc-tab label="one" minWidth isMinWidthIndicator></mwc-tab>
<mwc-tab label="two" minWidth isMinWidthIndicator></mwc-tab>
<mwc-tab label="three" minWidth isMinWidthIndicator></mwc-tab>
</mwc-tab-bar>

<h3>With Icons</h3>
<mwc-tab-bar activeIndex="2">
<mwc-tab label="one" icon="accessibility"></mwc-tab>
<mwc-tab label="two" icon="exit_to_app"></mwc-tab>
<mwc-tab label="three" icon="camera"></mwc-tab>
</mwc-tab-bar>

<h3>Only Icons</h3>
<mwc-tab-bar activeIndex="2">
<mwc-tab icon="accessibility"></mwc-tab>
<mwc-tab icon="exit_to_app"></mwc-tab>
<mwc-tab icon="camera"></mwc-tab>
</mwc-tab-bar>

<h3>Image Icons</h3>
<mwc-tab-bar activeIndex="2">
<mwc-tab hasImageIcon>
  <svg slot="icon" width="10px" height="10px">
    <circle
        r="5px"
        cx="5px"
        cy="5px"
        color="red">
    </circle>
  </svg>
</mwc-tab>
<mwc-tab hasImageIcon>
  <svg slot="icon" width="10px" height="10px">
    <rect
        width="10px"
        height="10px"
        color="green">
    </rect>
  </svg>
</mwc-tab>
<mwc-tab hasImageIcon>
  <svg slot="icon" width="14.143px" height="14.143px">
    <rect
        width="10px"
        height="10px"
        color="blue"
        y="2.071px"
        x="2.071px"
        style="transform:rotate(45deg);transform-origin:center;">
    </rect>
  </svg>
</mwc-tab>
</mwc-tab-bar>

<h3>With Icons - Stacked</h3>
<mwc-tab-bar activeIndex="1">
<mwc-tab label="one" icon="accessibility" stacked></mwc-tab>
<mwc-tab label="two" icon="exit_to_app" stacked></mwc-tab>
<mwc-tab label="three" icon="camera" stacked></mwc-tab>
</mwc-tab-bar>

<h3>With Icons - Stacked - Min Width Indicator</h3>
<mwc-tab-bar>
<mwc-tab label="one" icon="accessibility" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="two" icon="exit_to_app" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="three" icon="camera" stacked isMinWidthIndicator></mwc-tab>
</mwc-tab-bar>

<h3>Scrolling</h3>
<mwc-tab-bar>
<mwc-tab label="one"></mwc-tab>
<mwc-tab label="two"></mwc-tab>
<mwc-tab label="three"></mwc-tab>
<mwc-tab label="four"></mwc-tab>
<mwc-tab label="five"></mwc-tab>
<mwc-tab label="six"></mwc-tab>
<mwc-tab label="seven"></mwc-tab>
<mwc-tab label="eight"></mwc-tab>
<mwc-tab label="nine"></mwc-tab>
<mwc-tab label="ten"></mwc-tab>
</mwc-tab-bar>

<h3>Scrolling - Width Icons - Stacked - Min Width Indicator</h3>
<mwc-tab-bar>
<mwc-tab label="one" icon="camera" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="two" icon="camera" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="three" icon="camera" stacked isMinWidthIndicator ></mwc-tab>
<mwc-tab label="four" icon="camera" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="five" icon="camera" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="six" icon="camera" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="seven" icon="camera" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="eight" icon="camera" stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="nine" icon="camera"  stacked isMinWidthIndicator></mwc-tab>
<mwc-tab label="ten" icon="camera" stacked isMinWidthIndicator></mwc-tab>
</mwc-tab-bar>

<h3>Icon indicator</h3>
<mwc-tab-bar>
<mwc-tab icon="camera" isFadingIndicator indicatorIcon="donut_large"></mwc-tab>
<mwc-tab icon="accessibility" isFadingIndicator indicatorIcon="donut_large"></mwc-tab>
<mwc-tab icon="exit_to_app" isFadingIndicator indicatorIcon="donut_large"></mwc-tab>
</mwc-tab-bar>
`

export const Tabs = MwcTabsTemplate.bind({})

const MwcTextAreaTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
<h4>Textarea</h4>
<div class="demo-group-spaced">
  <mwc-textarea label="Standard"></mwc-textarea>
  <mwc-textarea outlined label="Standard"></mwc-textarea>
</div>

<h4>Textarea with Character Counter</h4>
<div class="demo-group-spaced">
  <mwc-textarea label="Standard" maxlength="18" charCounter></mwc-textarea>
  <mwc-textarea outlined label="Standard" maxlength="18" charCounter></mwc-textarea>
</div>

<h4>Textarea with Helper Text</h4>
<div class="demo-group-spaced">
  <mwc-textarea label="Standard" helper="Helper Text" helperPersistent></mwc-textarea>
  <mwc-textarea outlined label="Standard" helper="Helper Text" helperPersistent></mwc-textarea>
</div>

<h4>End-aligned</h4>
<div class="demo-group-spaced">
  <mwc-textarea label="Standard" value="End-aligned" endaligned></mwc-textarea>
  <mwc-textarea outlined label="Standard" value="End-aligned" endaligned></mwc-textarea>
</div>

<h4>Full Width Textarea</h4>
<mwc-textarea fullwidth placeholder="Full width" helper="Helper Text" helperpersistent></mwc-textarea>
<mwc-textarea outlined fullwidth placeholder="Full width" helper="Helper Text" helperpersistent></mwc-textarea>
`

export const Textareas = MwcTextAreaTemplate.bind({})

const MwcTextFieldTemplate: Story<ArgTypes> = ({
  title = 'Hello world',
  counter = 5,
  textColor,
  slot
}: ArgTypes) => html`
<h4>Filled</h4>
    <div class="demo-group-spaced">
      <mwc-textfield label="Standard"></mwc-textfield>
      <mwc-textfield label="Standard" icon="event"></mwc-textfield>
      <mwc-textfield label="Standard" iconTrailing="delete"></mwc-textfield>
    </div>

    <h4>Shaped Filled</h4>
    <div class="demo-group-spaced shaped-filled">
      <mwc-textfield label="Standard"></mwc-textfield>
      <mwc-textfield label="Standard" icon="event"></mwc-textfield>
      <mwc-textfield class="color" label="Standard" iconTrailing="delete"></mwc-textfield>
    </div>

    <h4>Outlined</h4>
    <div class="demo-group-spaced">
      <mwc-textfield outlined label="Standard"></mwc-textfield>
      <mwc-textfield outlined label="Standard" icon="event"></mwc-textfield>
      <mwc-textfield outlined label="Standard" iconTrailing="delete"></mwc-textfield>
    </div>

    <h4>Shaped Outlined</h4>
    <div class="demo-group-spaced shaped-outlined">
      <mwc-textfield outlined label="Email" type="email"></mwc-textfield>
      <mwc-textfield outlined label="Standard" icon="event"></mwc-textfield>
      <mwc-textfield outlined label="Standard" iconTrailing="delete"></mwc-textfield>
    </div>

    <h4>Text Field without label</h4>
    <div class="demo-group-spaced">
      <mwc-textfield helper="Helper Text"></mwc-textfield>
      <mwc-textfield outlined helper="Helper Text"></mwc-textfield>
      <mwc-textfield outlined helper="Helper Text" class="shaped-outlined"></mwc-textfield>
    </div>

    <h4>Text Field with Character Counter</h4>
    <div class="demo-group-spaced">
      <mwc-textfield label="Standard" helper="Helper Text" helperPersistent maxlength="18" charCounter></mwc-textfield>
      <mwc-textfield outlined label="Standard" helper="Helper Text" helperPersistent maxlength="18" charCounter></mwc-textfield>
      <mwc-textfield outlined label="Standard" helper="Helper Text" helperPersistent maxlength="18" charCounter class="shaped-outlined"></mwc-textfield>
    </div>

    <h4>End-aligned</h4>
    <div class="demo-group-spaced">
      <mwc-textfield label="Standard" value="End-aligned" endaligned></mwc-textfield>
      <mwc-textfield outlined label="Standard" value="End-aligned" endaligned></mwc-textfield>
      <mwc-textfield outlined label="Standard" value="End-aligned" endaligned class="shaped-outlined"></mwc-textfield>
    </div>

    <h4>Full Width</h4>
    <mwc-textfield fullwidth placeholder="Fullwidth" helper="Helper Text" helperpersistent></mwc-textfield>
`

export const Textfields = MwcTextFieldTemplate.bind({})
